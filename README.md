# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Git session for Kinetic Growth

### How do I get set up? ###

* Make sure you have Java, ANT, and Git installed on your system.
* Create a new Developer Salesforce org
* Fork this repo and checkout master
* Configure build.properties
* Make a Mavensmate project and deploy this repo to your dev org


### Usage ###
This repo uses the ANT Retrieve task as an alternative to Mavens Refresh from Server. This task then automatically commits the changes from the Retrieve task to your Git repo for this project.

**ant git-commit**

* Make sure gt.commitMsg and gt.directory are configured in build.properties
* Retrieves all files in src/package.xml
* Makes a Git commit with your build.properties commit message

### Who do I talk to? ###

* Repo owner or admin